package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by CCA_Student on 21/11/2018.
 */
public class BaseTests{

    WebDriver driver;

@BeforeTest
public void setUp(){
    driver = Utils.getDriver();}

@AfterTest
public void tearDown(){
driver.quit();
 }
}


