package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by CCA_Student on 21/11/2018.
 */
public class LoginTest extends BaseTest {
    @Test
    public void loginTest() {
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "Error");
        loginPage.login("tester", "letmein");
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "Error");
    }
}

