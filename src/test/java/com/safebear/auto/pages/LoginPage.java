package com.safebear.auto.pages;


import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import org.openqa.selenium.WebDriver;

/**
 * Created by CCA_Student on 21/11/2018.
 */

public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();
    @NonNull
    WebDriver driver;

    public String getPageTitle() {

        return driver.getTitle();
    }

    public void login(String uname, String pwrd) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(uname);
        driver.findElement(locators.getPasswordLocator()).sendKeys(pwrd);
        driver.findElement(locators.getLoginLocator()).click();

    }


}
