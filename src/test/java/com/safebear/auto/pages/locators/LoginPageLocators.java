package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

/**
 * Created by CCA_Student on 21/11/2018.
 */
@Data
public class LoginPageLocators {
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.id("password");
    private By LoginLocator = By.id("enter");
}
